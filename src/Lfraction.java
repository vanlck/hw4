//Kasutatud materjal:
//http://stackoverflow.com/questions/474535/best-way-to-represent-a-fraction-in-java
//http://stackoverflow.com/questions/5993779/java-use-split-with-multiple-delimiters
////https://www.sitepoint.com/how-to-implement-javas-hashcode-correctly/
import java.util.*;

/** This class represents fractions of form n/d where n and d are long integer 
 * numbers. Basic operations and arithmetics for fractions are provided.
 */

public class Lfraction implements Comparable<Lfraction> {
	
	private long lug;
	private long nim;

   /** Main method. Different tests. */
   public static void main (String[] param) {
      // TODO!!! Your debugging tests here
	   Lfraction f1 = new Lfraction (2, 4);
	   Lfraction f3 = new Lfraction (1, 4);
	   System.out.println(f1.divideBy(f3));
	   System.out.println(f1.plus(f3));
	   System.out.println(f1.equals(f3));
	   System.out.println(f1.compareTo(f3));
	   System.out.println ("lugeja: " + f1.getNumerator());
       System.out.println ("nimetaja: " + f3.getDenominator());
	   System.out.println(valueOf("			"));	 
   }

   // TODO!!! instance variables here

   /** Constructor.
    * @param a numerator
    * @param b denominator > 0
    */
   public Lfraction (long a, long b) {
      // TODO!!!
	   lug=a;
	   nim=b;
	   
	   if (nim == 0) {
		   throw new RuntimeException("Murru nimeja ei saa olla null!");
	   }
	   if (nim < 0) {
		  nim*=-1;
		  lug*=-1;		   
	   }
	   taandame ();
   }
   
   private static long syt(long a, long b){ // Eukleidese algoritm suurima ühisteguri leidmiseks
		  if (b==0){
			  return a;
		  } else {
			  return syt (b,a%b);
		  }	  
	   }
   
   private void taandame (){
	   long s= syt(Math.abs(lug), Math.abs(nim));
	   lug/= s;
	   nim/= s;
	   
   } 

   /** Public method to access the numerator field. 
    * @return numerator
    */
   public long getNumerator() {
      return lug; // TODO!!!
   }

   /** Public method to access the denominator field. 
    * @return denominator
    */
   public long getDenominator() { 
      return nim; // TODO!!!
   }

   /** Conversion to string.
    * @return string representation of the fraction
    */
   @Override
   public String toString() {
	  return Long.toString(lug) +"/"+Long.toString(nim);  // TODO!!!
   }

   /** Equality test.
    * @param m second fraction
    * @return true if fractions this and m are equal
    */
   @Override
   public boolean equals (Object m) {
	   Lfraction other = (Lfraction) m;
		  if (lug==other.lug && nim==other.nim){
			return true;  
		  } 
		  return false; // TODO!!!
   }

   /** Hashcode has to be equal for equal fractions.
    * @return hashcode
    */
   @Override
   public int hashCode() {
       // TODO!!!
	   return Objects.hash(this.lug, this.nim);
   }

   /** Sum of fractions.
    * @param m second addend
    * @return this+m
    */
   public Lfraction plus (Lfraction m) {
	   return new Lfraction((this.lug*m.nim)+(m.lug*this.nim),(this.nim*m.nim)); // TODO!!!
   }

   /** Multiplication of fractions.
    * @param m second factor
    * @return this*m
    */
   public Lfraction times (Lfraction m) {
	   // TODO!!!
	   return new Lfraction (lug*m.lug,nim*m.nim);
   }

   /** Inverse of the fraction. n/d becomes d/n.
    * @return inverse of this fraction: 1/this
    */
   public Lfraction inverse() {
	   if (nim==0) {
			throw new ArithmeticException("Murrus " + lug + "/" + nim + " ei tohi murru ümber pööramisel lugeja olla 0 ");
		} else {
			return new Lfraction(nim, lug); 
		} // TODO!!!
   }

   /** Opposite of the fraction. n/d becomes -n/d.
    * @return opposite of this fraction: -this
    */
   public Lfraction opposite() {
       // TODO!!!
	   return new Lfraction(lug*(-1), nim);
   }

   /** Difference of fractions.
    * @param m subtrahend
    * @return this-m
    */
   public Lfraction minus (Lfraction m) {
	   return plus(m.opposite());// TODO!!!
   }

   /** Quotient of fractions.
    * @param m divisor
    * @return this/m
    */
   public Lfraction divideBy (Lfraction m) {
	   if (m.nim==0) {
			throw new ArithmeticException("Murrus " + lug + "/" + nim + ", mis on jagatises jagaja, ei tohi lugeja olla 0 ");
		} else {
			Lfraction jagatis =new Lfraction (lug*m.nim,nim*m.lug);
			jagatis.taandame();
			return jagatis;
		} // TODO!!!
   }

   /** Comparision of fractions.
    * @param m second fraction
    * @return -1 if this < m; 0 if this==m; 1 if this > m
    */
   @Override
   public int compareTo (Lfraction m) {
	 //http://stackoverflow.com/questions/474535/best-way-to-represent-a-fraction-in-java
	 		long t = lug * m.nim;
	 		long f = m.lug * nim;
	 		int result = 0;
	 		if(t>f) {
	 			result = 1;
	 		}
	 		else if(f>t) {
	 			result = -1;
	 		}
	 		return result; // TODO!!!
   }

   /** Clone of the fraction.
    * @return new fraction equal to this
    */
   @Override
   public Object clone() throws CloneNotSupportedException {
      // TODO!!!
	  return new Lfraction(lug, nim);
   }

   /** Integer part of the (improper) fraction. 
    * @return integer part of this fraction
    */
   public long integerPart() {
      // TODO!!!
	  return lug/nim;
   }

   /** Extract fraction part of the (improper) fraction
    * (a proper fraction without the integer part).
    * @return fraction part of this fraction
    */
   public Lfraction fractionPart() {
      // TODO!!!
	  return new Lfraction(lug-integerPart()*nim, nim);
   }

   /** Approximate value of the fraction.
    * @return numeric value of this fraction
    */
   public double toDouble() {
      // TODO!!!
	  return (double)lug/(double)nim;
   }

   /** Double value f presented as a fraction with denominator d > 0.
    * @param f real number
    * @param d positive denominator for the result
    * @return f as an approximate fraction of form n/d
    */
   public static Lfraction toLfraction (double f, long d) {
      // TODO!!!
	   return new Lfraction (Math.round(((double)d*f)), d);
   }

   /** Conversion from string to the fraction. Accepts strings of form
    * that is defined by the toString method.
    * @param s string form (as produced by toString) of the fraction
    * @return fraction represented by s
    */
 //http://stackoverflow.com/questions/5993779/java-use-split-with-multiple-delimiters
   public static Lfraction valueOf(String s) {
		boolean isEmpty = s == null || s.trim().length() == 0;
		if (isEmpty) {
			throw new RuntimeException("Avaldis \"" + s + "\" on tühi!");
		}
		// Kui tagastatav arv väiksem kui null, siis / puudub stringist
		if (s.indexOf('/') < 0) {
			throw new RuntimeException("Avaldis \"" + s + "\" ei ole korrektne! Murd peab sisaldama sümbolit \"/\"!");
		}
		// Jaotame stringi tükkideks sümboli "/" järgi
		String[] parts = s.split("/");
		try {
			Long.parseLong(parts[0]);
			Long.parseLong(parts[1]);
		} catch (NumberFormatException e) {
			throw new RuntimeException("Avaldis \"" + s + "\" ei ole korrektne! Murd sisaldab keelatud sümboleid!");
		} catch (Exception f) {
			throw new RuntimeException("Avaldis \"" + s + "\" ei ole korrektne! Murrus puudub nimetaja ja/või lugeja!");
		}

		if (Long.parseLong(parts[1]) == 0) {
			throw new ArithmeticException("Avaldis \"" + s + "\" ei ole korrektne! Murru nimetaja ei saa null olla!");
		}
		// Tagastame uue murru, kus võtame parameetriteks stringi nullinda ja
		// esimese indeksi
		return new Lfraction(Long.parseLong(parts[0]), Long.parseLong(parts[1]));
	}
}

